/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer;

import javafx.application.Application;
import javafx.stage.Stage;
import net.worldsynth.composer.brush.BrushRegistry;
import net.worldsynth.composer.tool.ToolRegistry;
import net.worldsynth.composer.ui.WorldSynthComposer;

public class WorldSynthComposerLauncher extends Application {
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		new BrushRegistry();
		new ToolRegistry();
		new WorldSynthComposer(primaryStage);
	}
}
