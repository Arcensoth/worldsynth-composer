/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composition.layer.LayerNull;

public class LayerRenderNull extends LayerRender<LayerNull> {

	public LayerRenderNull(LayerNull layer) {
		super(layer);
	}

	@Override
	protected void updateRender(LayerNull layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.clearRect(0, 0, 10000, 10000);
	}

	@Override
	protected Pane renderParameterPane() {
		return new Pane();
	}

}
