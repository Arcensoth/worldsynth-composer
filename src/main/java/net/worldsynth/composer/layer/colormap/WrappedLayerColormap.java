package net.worldsynth.composer.layer.colormap;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import net.worldsynth.composer.layer.IWrappedLayer;
import net.worldsynth.composer.layer.LayerRender;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.colormap.LayerColormap;

public class WrappedLayerColormap extends LayerColormap implements IWrappedLayer<LayerColormap> {
	
	private final LayerRender<LayerColormap> render = new LayerRenderColormap(this);
	
	private final SimpleStringProperty propertyLayerName;
	private final SimpleObjectProperty<BlendMode> propertyLayerPreviewBlendMode;
	private final SimpleObjectProperty<Image> propertyLayerTumbnail;
	private final SimpleBooleanProperty propertyLayerLocked;
	private final SimpleBooleanProperty propertyLayerActive;


	public WrappedLayerColormap(Composition parentLayout, String name) {
		super(parentLayout, name);
		
		propertyLayerName = new SimpleStringProperty(name);
		propertyLayerPreviewBlendMode = new SimpleObjectProperty<BlendMode>(BlendMode.ADD);
		propertyLayerTumbnail = new SimpleObjectProperty<Image>(getDefaultLayerTumbnail());
		propertyLayerLocked = new SimpleBooleanProperty(false);
		propertyLayerActive = new SimpleBooleanProperty(true);
		
		propertyLayerName.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
			super.setName(newValue);
		});
		propertyLayerPreviewBlendMode.addListener((ObservableValue<? extends BlendMode> observable, BlendMode oldValue, BlendMode newValue) -> {
			super.setPreviewBlendMode(newValue);
		});
		propertyLayerLocked.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			super.setLocked(newValue);
		});
		propertyLayerActive.addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			super.setActive(newValue);
		});
	}

	@Override
	public Image getDefaultLayerTumbnail() {
		return new Image(getClass().getResourceAsStream("Color.png"));
	}

	@Override
	public LayerRender<LayerColormap> getLayerRender() {
		return render;
	}

	@Override
	public SimpleStringProperty layerNameProperty() {
		return propertyLayerName;
	}

	@Override
	public SimpleObjectProperty<BlendMode> layerPreviewBlendmodeProperty() {
		return propertyLayerPreviewBlendMode;
	}

	@Override
	public SimpleObjectProperty<Image> layerTumbnailProperty() {
		return propertyLayerTumbnail;
	}

	@Override
	public SimpleBooleanProperty layerActiveProperty() {
		return propertyLayerActive;
	}

	@Override
	public SimpleBooleanProperty layerLockedProperty() {
		return propertyLayerLocked;
	}
}
