package net.worldsynth.composer.layer;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import net.worldsynth.composition.layer.Layer;

public interface IWrappedLayer<T extends Layer> {
	
	public Image getDefaultLayerTumbnail();
	public LayerRender<T> getLayerRender();
	
	public SimpleStringProperty layerNameProperty();
	public SimpleObjectProperty<BlendMode> layerPreviewBlendmodeProperty();
	public SimpleObjectProperty<Image> layerTumbnailProperty();
	public SimpleBooleanProperty layerActiveProperty();
	public SimpleBooleanProperty layerLockedProperty();
	
	public default void setLayerName(String layerName) {
		layerNameProperty().set(layerName);
	}
	public default String getLayerName() {
		return layerNameProperty().get();
	}
	
	public default void setLayerPreviewBlemndMode(BlendMode blendMode) {
		layerPreviewBlendmodeProperty().set(blendMode);
	}
	public default BlendMode getLayerPreviewBlendMode() {
		return layerPreviewBlendmodeProperty().get();
	}
	
	public default void setLayerTumbnail(Image layerTumbnail) {
		layerTumbnailProperty().set(layerTumbnail);
	}
	public default Image getLayerTumbnail() {
		return layerTumbnailProperty().get();
	}
	
	public default void setLayerActive(boolean layerActive) {
		layerActiveProperty().set(layerActive);;
	}
	public default boolean getLayerActive() {
		return layerActiveProperty().get();
	}
	
	public default void setLayerLocked(boolean layerLocked) {
		layerLockedProperty().set(layerLocked);
	}
	public default boolean getLayerLocked() {
		return layerLockedProperty().get();
	}
}
