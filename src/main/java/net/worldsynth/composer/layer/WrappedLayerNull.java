package net.worldsynth.composer.layer;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.LayerNull;

public class WrappedLayerNull extends LayerNull implements IWrappedLayer<LayerNull> {

	public WrappedLayerNull(Composition parrentLayout, String name) {
		super(parrentLayout, name);
	}

	@Override
	public Image getDefaultLayerTumbnail() {
		return null;
	}

	@Override
	public LayerRender<LayerNull> getLayerRender() {
		return null;
	}

	@Override
	public SimpleStringProperty layerNameProperty() {
		return null;
	}

	@Override
	public SimpleObjectProperty<BlendMode> layerPreviewBlendmodeProperty() {
		return null;
	}

	@Override
	public SimpleObjectProperty<Image> layerTumbnailProperty() {
		return null;
	}

	@Override
	public SimpleBooleanProperty layerActiveProperty() {
		return null;
	}

	@Override
	public SimpleBooleanProperty layerLockedProperty() {
		return null;
	}

}
