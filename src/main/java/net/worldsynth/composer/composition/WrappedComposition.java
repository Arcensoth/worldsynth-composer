/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.composition;

import java.io.File;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.worldsynth.composer.layer.heightmap.WrappedLayerHeightmap;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.LayerNull;

public class WrappedComposition extends Composition {
	
	private final SimpleStringProperty properyCompositionName;
	private final ObservableList<Layer> compositionLayers;
	
	private final SimpleObjectProperty<Layer> selectedLayer = new SimpleObjectProperty<Layer>(new LayerNull(this, "null"));
	
	public WrappedComposition(String name) {
		super(name);
		properyCompositionName = new SimpleStringProperty(super.getCompositionName());
		compositionLayers = FXCollections.observableArrayList(super.getLayersList());
		
		properyCompositionName.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
			super.setCompositionName(newValue);
		});
		
		compositionLayers.add(new WrappedLayerHeightmap(this, "Default layer"));
		setSelectedLayer(compositionLayers.get(0));
	}
	
	public WrappedComposition(File compositionStorage) {
		super(compositionStorage);
		
		properyCompositionName = new SimpleStringProperty(super.getCompositionName());
		compositionLayers = FXCollections.observableArrayList(super.getLayersList());
		
		properyCompositionName.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
			super.setCompositionName(newValue);
		});
		
		compositionLayers.add(new WrappedLayerHeightmap(this, "Default layer"));
		setSelectedLayer(compositionLayers.get(0));
	}
	
	public SimpleStringProperty compositionNameProperty() {
		return properyCompositionName;
	}
	
	public String getCompositionName() {
		return properyCompositionName.get();
	}
	
	public void setCompositionName(String projectName) {
		this.properyCompositionName.set(projectName);
	}
	
	public ObservableList<Layer> getObservableLayersList() {
		return compositionLayers;
	}
	
	public void addLayer(Layer layer) {
		compositionLayers.add(layer);
	}
	
	public boolean removeLayer(Layer layer) {
		return compositionLayers.remove(layer);
	}
	
	public SimpleObjectProperty<Layer> selectedLayerProperty() {
		return selectedLayer;
	}
	
	public Layer getSelectedLayer() {
		return selectedLayer.get();
	}
	
	public void setSelectedLayer(Layer selectedLayer) {
		this.selectedLayer.set(selectedLayer);
	}
}
