/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui;

import java.util.HashMap;

import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.worldsynth.composer.composition.WrappedComposition;
import net.worldsynth.composer.tool.ComposerToolBar;
import net.worldsynth.composer.tool.ToolRegistry;
import net.worldsynth.composer.ui.composition.CompositionViewer;
import net.worldsynth.composer.ui.layerbrowser.LayerBrowser;

public class WorldSynthComposer {
	//The main stage for WorldDesigner
	private final Stage stage;
	
	//Content
	public static MenuBar menuBar;
	public static final ComposerToolBar toolbar = new ComposerToolBar();;
	public static final TabPane projectTabPane = new TabPane();
	public static final CompositionViewer layoutViewer = new CompositionViewer();
	public static final BorderPane toolParametersPaneWrapper = new BorderPane();
	public static final LayerBrowser layersList = new LayerBrowser();
	
	//Projects
	private final HashMap<Tab, WrappedComposition> projects = new HashMap<Tab, WrappedComposition>();
	
	public WorldSynthComposer(Stage stage) throws Exception {
		this.stage = stage;
		stage.initStyle(StageStyle.DECORATED);
		stage.setTitle("WorldSynth Composer pre-alpha");
		stage.setMinWidth(800);
		stage.setMinHeight(700);
		Image stageIcon = new Image(getClass().getResourceAsStream("worldSynthIcon.png"));
		stage.getIcons().add(stageIcon);
		
		//Build up main UI
		//Add menu bar at top
		BorderPane root = new BorderPane();
		menuBar = new MenuBar(new Menu("File"), new Menu("Edit"), new Menu("Help"));
		root.setTop(menuBar);
		
		//Add toolbar at left side
		root.setLeft(toolbar);
		
		//Add project tabs
		projectTabPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
			setAsCurrentProject(projects.get(newValue));
		});
		root.setCenter(projectTabPane);
		
		//Add right side content
		
		BorderPane parametersAndLayersPane = new BorderPane();
		parametersAndLayersPane.setTop(toolParametersPaneWrapper);
		parametersAndLayersPane.setCenter(layersList);
		root.setRight(parametersAndLayersPane);
		
		//Setup the scene and style
		Scene scene = new Scene(root, 1500, 800);
		scene.getStylesheets().add(getClass().getResource("WorldDesignerMainStyle.css").toExternalForm());
		
		stage.setScene(scene);
		stage.show();
		

		addNewlayoutProject("Untitled project 1");
//		addNewlayoutProject("Untitled project 2");
//		addNewlayoutProject("Untitled project 3");
	}
	
	private void addNewlayoutProject(String layoutProjectName) {
		Tab projectTab = new Tab(layoutProjectName);
		WrappedComposition newLayoutProject = new WrappedComposition(layoutProjectName);
		projectTab.setContent(layoutViewer);
		projects.put(projectTab, newLayoutProject);
		projectTabPane.getTabs().add(projectTab);
	}
	
	private void setAsCurrentProject(WrappedComposition layoutProject) {
		layoutViewer.setLayoutProject(layoutProject);
		toolbar.setTools(ToolRegistry.getToolsForLayer(layoutProject.getSelectedLayer()));
		layersList.setLayoutProject(layoutProject);
	}
	
	public static void updatePreview() {
		layoutViewer.updatePreview();
	}
}
