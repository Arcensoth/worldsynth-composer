/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.layerbrowser;

import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.css.PseudoClass;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import net.worldsynth.composer.layer.IWrappedLayer;
import net.worldsynth.composition.layer.Layer;

public class LayerView extends HBox {
	
	private final Layer layer;
	
	private final LayerBrowser parentLayerBrowser;
	
	private final TextField layerNameField;
	private final Label layerNameLabel;
	private final CheckBox layerLockedCheckbox;
	private final CheckBox layerActiveCheckbox;
	
	public LayerView(Layer layer, LayerBrowser parentLayerBrowser) {
		this.layer = layer;
		this.parentLayerBrowser = parentLayerBrowser;
		IWrappedLayer<?> wrappedLayer = (IWrappedLayer<?>) layer;
		
		setSpacing(5.0);
		setAlignment(Pos.CENTER);
		setPrefWidth(260);
		getStyleClass().add("layer");
		
		ImageView layerTumbnailView = new ImageView();
		Bindings.bindBidirectional(layerTumbnailView.imageProperty(), wrappedLayer.layerTumbnailProperty());
		
		layerNameField = new TextField();
		HBox.setHgrow(layerNameField, Priority.ALWAYS);
		Bindings.bindBidirectional(layerNameField.textProperty(), wrappedLayer.layerNameProperty());
		layerNameLabel = new Label();
		Bindings.bindBidirectional(layerNameLabel.textProperty(), wrappedLayer.layerNameProperty());
		Pane fillPane = new Pane();
		HBox.setHgrow(fillPane, Priority.ALWAYS);
		
		layerLockedCheckbox = new CheckBox();
		layerLockedCheckbox.setSelected(wrappedLayer.getLayerLocked());
		Tooltip.install(layerLockedCheckbox, new Tooltip("Lock layer"));
		layerLockedCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			wrappedLayer.setLayerLocked(newValue);
		});
		
		layerActiveCheckbox = new CheckBox();
		layerActiveCheckbox.setSelected(wrappedLayer.getLayerActive());
		Tooltip.install(layerActiveCheckbox, new Tooltip("Show layer"));
		layerActiveCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			wrappedLayer.setLayerActive(newValue);
		});
		
		getChildren().setAll(layerTumbnailView, layerNameLabel, fillPane, layerLockedCheckbox, layerActiveCheckbox);
		
		setOnMouseClicked(e -> {
			parentLayerBrowser.setSelectedLayerView(this);
			requestFocus();
		});
	}
	
	void setSelected(boolean selected) {
		if(selected) {
			getChildren().set(1, layerNameField);
			pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), true);
		}
		else {
			getChildren().set(1, layerNameLabel);
			pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), false);
		}
	}
	
	public Layer getLayer() {
		return layer;
	}
}
