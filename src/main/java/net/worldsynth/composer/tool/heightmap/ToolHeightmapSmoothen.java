/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.heightmap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.composer.brush.heightmap.Heightbrush;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;

public class ToolHeightmapSmoothen extends ToolHeightmap {
	
	private final ParametersToolHeightmap parameters = new ParametersToolHeightmap();
	
	private boolean active = false;
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getResourceAsStream("toolicon_smoothen.png")));
	}
	
	@Override
	protected Pane getToolParameterPane() {
		return parameters;
	}
	
	@Override
	public void onPrimaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
		active = true;
		Heightbrush brush = parameters.brush;
		smothen((int) x - brush.getBrushWidth()/2, (int) z - brush.getBrushHeight()/2, pressure, brush, layer);
	}
	
	@Override
	public void onSecondaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
	}
	
	@Override
	public void onPrimaryDown(double x, double z, double pressure, LayerHeightmap layer) {
		Heightbrush brush = parameters.brush;
		smothen((int) x - brush.getBrushWidth()/2, (int) z - brush.getBrushHeight()/2, pressure, brush, layer);
	}
	
	@Override
	public void onSecondaryDown(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onPrimaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
		Heightbrush brush = parameters.brush;
		smothen((int) x - brush.getBrushWidth()/2, (int) z - brush.getBrushHeight()/2, pressure, brush, layer);
	}
	
	@Override
	public void onSecondaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onPrimaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
		active = false;
	}
	
	@Override
	public void onSecondaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas) {
		Heightbrush brush = parameters.brush;
		if(active || brush == null) return;
		
		Pixel corner = new Pixel(new Coordinate(x - brush.getBrushWidth()*0.5, z - brush.getBrushHeight()*0.5), navCanvas);
		g.drawImage(brush.getBrushImage(), corner.x, corner.y, brush.getBrushWidth()*navCanvas.getZoom(), brush.getBrushHeight()*navCanvas.getZoom());
		g.setStroke(Color.WHEAT);
		g.strokeRect(corner.x, corner.y, brush.getBrushWidth()*navCanvas.getZoom(), brush.getBrushHeight()*navCanvas.getZoom());
	}
	
	private void smothen(int x, int z, double pressure, Heightbrush brush, LayerHeightmap layer) {
		float[][] heights = new float[brush.getBrushWidth()][brush.getBrushHeight()];
		float[][] ch = new float[brush.getBrushWidth()+2][brush.getBrushHeight()+2];
		
		for(int u = 0; u < brush.getBrushWidth()+2; u++) {
			for(int v = 0; v < brush.getBrushHeight()+2; v++) {
				ch[u][v] = layer.getValueAt(x+u-1, z+v-1);
			}
		}
		
		for(int u = 0; u < brush.getBrushWidth(); u++) {
			for(int v = 0; v < brush.getBrushHeight(); v++) {
				float avg = (ch[u+1][v+1] + ch[u][v+1] + ch[u+2][v+1] + ch[u+1][v] + ch[u+1][v+2]) / 5;
				float brushValue = brush.getBrushValues()[u][v];
				heights[u][v] = avg * brushValue + ch[u+1][v+1] * (1.0f - brushValue);
			}
		}
		
		layer.applyValues(x, z, heights);
	}
}
