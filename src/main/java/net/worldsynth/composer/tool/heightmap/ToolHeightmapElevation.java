/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.heightmap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;
import net.worldsynth.common.math.Permutation;
import net.worldsynth.common.math.Vector2d;
import net.worldsynth.composer.brush.heightmap.Heightbrush;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;

public class ToolHeightmapElevation extends ToolHeightmap {
	
	private final ParametersToolHeightmap parameters = new ParametersToolHeightmap();
	
	private final Permutation rotationHashPermuation = new Permutation(256, 1);
	
	private boolean primaryActive = false;
	private boolean secondaryActive = false;
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getResourceAsStream("toolicon_elevation.png")));
	}
	
	@Override
	protected Pane getToolParameterPane() {
		return parameters;
	}

	@Override
	public void onPrimaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
		primaryActive = true;
		Heightbrush brush = parameters.brush;
		if(brush == null) {
			return;
		}
		double brushRotationDeg = parameters.angle;
		if(parameters.randomizeRotatation) {
			brushRotationDeg = rotationHashPermuation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		applyTransformedBrush(layer, brush, 0.1 * parameters.strength * pressure, x, z, 1.0, brushRotationDeg);
	}
	
	@Override
	public void onSecondaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
		secondaryActive = true;
		Heightbrush brush = parameters.brush;
		if(brush == null) {
			return;
		}
		double brushRotationDeg = parameters.angle;
		if(parameters.randomizeRotatation) {
			brushRotationDeg = rotationHashPermuation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		applyTransformedBrush(layer, brush, -0.1 * parameters.strength * pressure, x, z, 1.0, brushRotationDeg);
	}
	
	@Override
	public void onPrimaryDown(double x, double z, double pressure, LayerHeightmap layer) {
		Heightbrush brush = parameters.brush;
		if(brush == null) {
			return;
		}
		double brushRotationDeg = parameters.angle;
		if(parameters.randomizeRotatation) {
			brushRotationDeg = rotationHashPermuation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		applyTransformedBrush(layer, brush, 0.1 * parameters.strength * pressure, x, z, 1.0, brushRotationDeg);		
	}

	@Override
	public void onSecondaryDown(double x, double z, double pressure, LayerHeightmap layer) {
		Heightbrush brush = parameters.brush;
		if(brush == null) {
			return;
		}
		double brushRotationDeg = parameters.angle;
		if(parameters.randomizeRotatation) {
			brushRotationDeg = rotationHashPermuation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		applyTransformedBrush(layer, brush, -0.1 * parameters.strength * pressure, x, z, 1.0, brushRotationDeg);
	}

	@Override
	public void onPrimaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onSecondaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onPrimaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
		primaryActive = false;
	}

	@Override
	public void onSecondaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
		secondaryActive = false;
	}
	
	private void applyTransformedBrush(LayerHeightmap layer, Heightbrush brush, double strength, double x, double z, double scale, double rotationDeg) {
		//Convert rotation angle from deg to rad
		double rotationRad = Math.toRadians(rotationDeg);
		//Create the transform matrix for mapping points in the scatter heightmap to points in the feature heightmap
		double[][] inverse_transform = {
				{Math.cos(-rotationRad)/scale, -Math.sin(-rotationRad)/scale, (-x*Math.cos(-rotationRad)+z*Math.sin(-rotationRad))/scale + (double)brush.getBrushWidth()/2.0},
				{Math.sin(-rotationRad)/scale,  Math.cos(-rotationRad)/scale, (-x*Math.sin(-rotationRad)-z*Math.cos(-rotationRad))/scale + (double)brush.getBrushHeight()/2.0},
				{0                        , 0                         , 1          }};
		
		int[] bounds = bounds(x, z, brush.getBrushHeight(), brush.getBrushHeight(), scale, rotationRad);
		
		int minX = bounds[0];
		int maxX = bounds[1];
		int minZ = bounds[2];
		int maxZ = bounds[3];
		
		for(int u = minX; u < maxX; u++) {
			for(int v = minZ; v < maxZ; v++) {
				double[] sample = applyTransform(u, v, inverse_transform);
				if(!brush.isLocalContained((int) sample[0], (int) sample[1])) continue;
				layer.applyChangeAt(u, v, brush.getLocalLerpHeight(sample[0], sample[1]), (float)strength);
			}
		}
	}
	
	//Returns {minX, maxX, minZ, maxZ}
	private int[] bounds(double x, double z, double w, double l, double scale, double rotate) {
		//Create the transform matrix for mapping points in the feature heightmap to points in the scatter heightmap
		double[][] transform = {
				{scale*Math.cos(rotate), -scale*Math.sin(rotate), (-w*scale*Math.cos(rotate)+l*scale*Math.sin(rotate))/2+x},
				{scale*Math.sin(rotate),  scale*Math.cos(rotate), (-w*scale*Math.sin(rotate)-l*scale*Math.cos(rotate))/2+z},
				{0                     , 0                      , 1                                                       }};
		
		double[] c0 = applyTransform(0, 0, transform);
		double[] c1 = applyTransform(w, 0, transform);
		double[] c2 = applyTransform(w, l, transform);
		double[] c3 = applyTransform(0, l, transform);
		
		int minX = (int) Math.floor(min(c0[0], c1[0], c2[0], c3[0]));
		int maxX = (int) Math.floor(max(c0[0], c1[0], c2[0], c3[0]));
		int minZ = (int) Math.floor(min(c0[1], c1[1], c2[1], c3[1]));
		int maxZ = (int) Math.floor(max(c0[1], c1[1], c2[1], c3[1]));
		
		return new int[] {minX, maxX, minZ, maxZ};
	}
	
	private double[] applyTransform(double x, double z, double[][] transform) {
		return new double[] {
			x*transform[0][0] + z*transform[0][1] + transform[0][2],
			x*transform[1][0] + z*transform[1][1] + transform[1][2]
		};
	}
	
	private double min(double...values) {
		double min = values[0];
		
		for(double val: values) {
			min = Math.min(min, val);
		}
		
		return min;
	}
	
	private double max(double...values) {
		double max = values[0];
		
		for(double val: values) {
			max = Math.max(max, val);
		}
		
		return max;
	}
	
	@Override
	public void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas) {
		if(primaryActive || secondaryActive || parameters.brush == null) return;
		Pixel screenPixelCoordinate = new Pixel(new Coordinate(x, z), navCanvas);
		double brushRotationDeg = parameters.angle;
		if(parameters.randomizeRotatation) {
			brushRotationDeg = rotationHashPermuation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		drawRotatedBrush(screenPixelCoordinate.x, screenPixelCoordinate.y, brushRotationDeg, g, navCanvas);
	}
	
	private void drawRotatedBrush(double x, double z, double angle, GraphicsContext g, NavigationalCanvas navCanvas) {
		Affine a = g.getTransform();
		g.rotate(angle);
		
		Vector2d v = new Vector2d(x, z);
		v = Vector2d.rotateDeg(v, -angle);
		
		g.setStroke(Color.WHEAT);
		g.drawImage(parameters.brush.getBrushImage(), v.getX() - parameters.brush.getBrushWidth()*navCanvas.getZoom()/2.0, v.getY() - parameters.brush.getBrushWidth()*navCanvas.getZoom()/2.0, parameters.brush.getBrushWidth()*navCanvas.getZoom(), parameters.brush.getBrushHeight()*navCanvas.getZoom());
		g.strokeRect(v.getX() - parameters.brush.getBrushWidth()*navCanvas.getZoom()/2.0, v.getY() - parameters.brush.getBrushWidth()*navCanvas.getZoom()/2.0, parameters.brush.getBrushWidth()*navCanvas.getZoom(), parameters.brush.getBrushHeight()*navCanvas.getZoom());
		
		g.setTransform(a);
	}
}
