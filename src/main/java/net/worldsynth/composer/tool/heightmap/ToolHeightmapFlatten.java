/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.heightmap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.composer.brush.heightmap.Heightbrush;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;

public class ToolHeightmapFlatten extends ToolHeightmap {
	
	private final ParametersToolHeightmap parameters = new ParametersToolHeightmap();
	
	private boolean active = false;
	
	float currentHeightSample;
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getResourceAsStream("toolicon_flatten.png")));
	}
	
	@Override
	protected Pane getToolParameterPane() {
		return parameters;
	}

	@Override
	public void onPrimaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
		currentHeightSample = layer.getValueAt((int) x, (int) z);
	}

	@Override
	public void onSecondaryPressed(double x, double z, double pressure, LayerHeightmap layer) {
	}
	
	@Override
	public void onPrimaryDown(double x, double z, double pressure, LayerHeightmap layer) {
		Heightbrush b = parameters.brush;
		flatten((int) x - b.getBrushWidth()/2, (int) z - b.getBrushHeight()/2, pressure, b, layer);
	}

	@Override
	public void onSecondaryDown(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onPrimaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
		Heightbrush b = parameters.brush;
		flatten((int) x - b.getBrushWidth()/2, (int) z - b.getBrushHeight()/2, pressure, b, layer);
	}

	@Override
	public void onSecondaryDragged(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onPrimaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void onSecondaryReleased(double x, double z, double pressure, LayerHeightmap layer) {
	}

	@Override
	public void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas) {
		Heightbrush brush = parameters.brush;
		if(active || brush == null) return;
		
		Pixel corner = new Pixel(new Coordinate(x - brush.getBrushWidth()*0.5, z - brush.getBrushHeight()*0.5), navCanvas);
		g.drawImage(brush.getBrushImage(), corner.x, corner.y, brush.getBrushWidth()*navCanvas.getZoom(), brush.getBrushHeight()*navCanvas.getZoom());
		g.setStroke(Color.WHEAT);
		g.strokeRect(corner.x, corner.y, brush.getBrushWidth()*navCanvas.getZoom(), brush.getBrushHeight()*navCanvas.getZoom());
	}
	
	private void flatten(int x, int z, double pressure, Heightbrush brush, LayerHeightmap layer) { 
		for(int u = 0; u < brush.getBrushWidth(); u++) {
			for(int v = 0; v < brush.getBrushHeight(); v++) {
				float isValue = layer.getValueAt(x+u, z+v);
				float setValue = currentHeightSample;
				float diff = setValue - isValue;
				layer.applyValueAt(x+u, z+v, isValue + diff * brush.getBrushValues()[u][v] * 0.1f * (float) pressure);
			}
		}
	}
}
