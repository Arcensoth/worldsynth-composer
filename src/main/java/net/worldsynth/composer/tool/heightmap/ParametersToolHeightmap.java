package net.worldsynth.composer.tool.heightmap;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import net.worldsynth.composer.brush.Brush;
import net.worldsynth.composer.brush.heightmap.Heightbrush;
import net.worldsynth.composer.ui.brushbrowser.BrushBrowser;

public class ParametersToolHeightmap extends BorderPane {
	
	public Heightbrush brush = null;
	public double strength = 1.0f;
	public double angle = 0.0f;
	public boolean randomizeRotatation = false;
	
	public ParametersToolHeightmap() {
		TabPane tabPane = new TabPane();
		
		//Brush
		BrushBrowser brushBrowser = new BrushBrowser();
		brushBrowser.selectedBrushProperty().addListener((ObservableValue<? extends Brush> observable, Brush oldValue, Brush newValue) -> {
			brush = (Heightbrush) newValue;
		});
		tabPane.getTabs().add(new Tab("Brush", brushBrowser));
		
		//Parameters
		GridPane parameterPane = new GridPane();
		Slider strengthSlider = new Slider(0.0, 1.0, strength);
		Slider angleSlider = new Slider(0.0, 360.0, angle);
		CheckBox randomizeRotationCheckboBox = new CheckBox();
		randomizeRotationCheckboBox.setSelected(randomizeRotatation);
		
		strengthSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			strength = (double) newValue;
		});
		
		angleSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
			angle = (double) newValue;
		});
		
		randomizeRotationCheckboBox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			randomizeRotatation = (boolean) newValue;
		});
		
		parameterPane.add(new Label("Strength: "), 0, 0);
		parameterPane.add(strengthSlider, 1, 0);
		parameterPane.add(new Label("Angle: "), 0, 1);
		parameterPane.add(angleSlider, 1, 1);
		parameterPane.add(new Label("RandomizeRotation: "), 0, 2);
		parameterPane.add(randomizeRotationCheckboBox, 1, 2);
		
		tabPane.getTabs().add(new Tab("Parameters", parameterPane));
		
		setCenter(tabPane);
	}
	
}
