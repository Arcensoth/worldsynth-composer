/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool;

import java.util.ArrayList;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import net.worldsynth.composer.ui.WorldSynthComposer;

public class ComposerToolBar extends BorderPane {
	
	ToolBar toolbar;
	Tool<?> selectedTool = new ToolCursor();
	
	public ComposerToolBar() {
		toolbar = new ToolBar();
		toolbar.setOrientation(Orientation.VERTICAL);
		setCenter(toolbar);
	}
	
	public void setTools(ArrayList<Tool<?>> tools) {
		toolbar.getItems().clear();
		for(Tool<?> t: tools) {
			Button toolButton = new Button();
			toolButton.setGraphic(new ImageView(t.getToolIconImage()));
			toolButton.setOnAction(e -> {
				selectedTool = t;
				WorldSynthComposer.toolParametersPaneWrapper.setCenter(t.getToolParameterPane());
			});
			toolbar.getItems().add(toolButton);
		}
		selectedTool = tools.get(0);
	}
	
	public Tool<?> getSelectedTool() {
		return selectedTool;
	}
}
