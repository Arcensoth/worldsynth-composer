/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composition.layer.Layer;

public abstract class Tool<T extends Layer> {
	private Image toolIconImage;
	
	public Tool() {
		toolIconImage = toolIconImage();
	}
	
	public Image getToolIconImage() {
		return toolIconImage;
	}
	
	public final T castLayer(Layer l) {
		return (T) l;
	}
	
	public abstract boolean isApplicableToLayer(Layer layer);
	
	protected abstract Image toolIconImage();
	
	protected abstract Pane getToolParameterPane();
	
	public abstract void onPrimaryPressed(double x, double z, double pressure, T layer);
	public abstract void onSecondaryPressed(double x, double z, double pressure, T layer);
	public abstract void onPrimaryDown(double x, double z, double pressure, T layer);
	public abstract void onSecondaryDown(double x, double z, double pressure, T layer);
	public abstract void onPrimaryDragged(double x, double z, double pressure, T layer);
	public abstract void onSecondaryDragged(double x, double z, double pressure, T layer);
	public abstract void onPrimaryReleased(double x, double z, double pressure, T layer);
	public abstract void onSecondaryReleased(double x, double z, double pressure, T layer);
	
	public abstract void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas);
}
