package net.worldsynth.composer.tool;

import java.util.ArrayList;

import net.worldsynth.composer.tool.heightmap.ToolHeightmapElevation;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapFlatten;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapSmoothen;
import net.worldsynth.composition.layer.Layer;

public class ToolRegistry {
	
	public static ArrayList<Tool<?>> REGISTER = new ArrayList<Tool<?>>();
	
	public ToolRegistry() {
		registerTools();
	}
	
	private void registerTools() {
		//Null tools
		REGISTER.add(new ToolCursor());
		
		//Heightmap tools
		REGISTER.add(new ToolHeightmapElevation());
		REGISTER.add(new ToolHeightmapFlatten());
		REGISTER.add(new ToolHeightmapSmoothen());
	}
	
	public static ArrayList<Tool<?>> getToolsForLayer(Layer layer) {
		ArrayList<Tool<?>> layerTools = new ArrayList<Tool<?>>();
		
		for(Tool<?> t: REGISTER) {
			if(t.isApplicableToLayer(layer)) {
				layerTools.add(t);
			}
		}
		
		return layerTools;
	}
}
