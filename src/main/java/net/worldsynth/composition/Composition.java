/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition;

import java.io.File;
import java.util.ArrayList;

import net.worldsynth.composition.layer.Layer;

public class Composition {
	
	private String compositionName;
	private final ArrayList<Layer> compositionLayers;
	
	public Composition(String name) {
		this.compositionName = name;
		compositionLayers = new ArrayList<Layer>();
	}
	
	public Composition(File compositionStorage) {
		compositionLayers = new ArrayList<Layer>();
		readCompositionFromFile(compositionStorage);
	}
	
	public String getCompositionName() {
		return compositionName;
	}
	
	public void setCompositionName(String projectName) {
		this.compositionName = projectName;
	}
	
	public ArrayList<Layer> getLayersList() {
		return compositionLayers;
	}
	
	public void writeCompositionToFile(File f) {
		//TODO Write layout to file
	}
	
	public void readCompositionFromFile(File f) {
		//TODO Read layout from file
	}
}
