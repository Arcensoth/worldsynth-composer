/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer;

import java.io.File;

import javafx.scene.effect.BlendMode;
import net.worldsynth.composition.Composition;

public abstract class Layer {
	
	private final Composition parentLayout;
	
	private String layerName = "Unnamed layer";
	private BlendMode layerPreviewBlendMode = BlendMode.ADD;
	private boolean layerLocked = false;
	private boolean layerActive = true;
	
	public Layer(Composition parentLayout, String name) {
		this.parentLayout = parentLayout;
		this.layerName = name;
	}
	
	public Composition getParentLayout() {
		return parentLayout;
	}
	
	public void setName(String layerName) {
		this.layerName = layerName;
	}
	
	public String getName() {
		return layerName;
	}
	
	public void setPreviewBlendMode(BlendMode blendMode) {
		this.layerPreviewBlendMode = blendMode;
	}
	
	public BlendMode getPreviewBlendMode() {
		return layerPreviewBlendMode;
	}
	
	public void setActive(boolean layerActive) {
		this.layerActive = layerActive;
	}
	
	public boolean getActive() {
		return layerActive;
	}
	
	public void setLocked(boolean layerLocked) {
		this.layerLocked = layerLocked;
	}
	
	public boolean getLocked() {
		return layerLocked;
	}
	
	public abstract void writeToFile(File f);
	public abstract void readFromFile(File f);
}
