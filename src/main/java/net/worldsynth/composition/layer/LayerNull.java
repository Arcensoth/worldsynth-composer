/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer;

import java.io.File;

import net.worldsynth.composition.Composition;

public class LayerNull extends Layer {

	public LayerNull(Composition parrentLayout, String name) {
		super(parrentLayout, name);
	}

	@Override
	public void writeToFile(File f) {
		return;
	}

	@Override
	public void readFromFile(File f) {
		return;
	}
}
