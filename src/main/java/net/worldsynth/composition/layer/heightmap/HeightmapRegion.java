/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.heightmap;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.custom.FloatArrayTag;

public class HeightmapRegion {
	
	public final int regionX, regionZ;
	public float[][] regionValues = new float[256][256];
	
	private boolean rebuildRenderQued = true;
	
	public HeightmapRegion(int regionX, int regionZ) {
		this.regionX = regionX;
		this.regionZ = regionZ;
		
		queRebuildRender();
	}
	
	public final int getRegionX() {
		return regionX;
	}
	
	public final int getRegionZ() {
		return regionZ;
	}
	
	public final float getHeightAt(int localX, int localZ) {
		return regionValues[localX][localZ];
	}
	
	public final void queRebuildRender() {
		rebuildRenderQued = true;
	}
	
	public final void edqueRebuildRender() {
		rebuildRenderQued = false;
	}
	
	public final boolean regionRebuildRenderIsQued() {
		return rebuildRenderQued;
	}
	
	public final CompoundTag toNbt() {
		CompoundTag regionCompoundTag = new CompoundTag("Region");
		
		float[] values = new float[256*256];
		for(int i = 0; i < 256; i++) {
			for(int j = 0; j < 256; j++) {
				values[i*256+j] = regionValues[i][j];
			}
		}
		regionCompoundTag.put(new FloatArrayTag("Values", values));
		
		return regionCompoundTag;	}
	
	public final void fromNbt(CompoundTag nbt) {
		
	}
}
