/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.heightmap;

import java.io.File;
import java.util.HashMap;

import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;

public class LayerHeightmap extends Layer {
	
	public final HashMap<String, HeightmapRegion> regionmap = new HashMap<String, HeightmapRegion>(256);
	
	public LayerHeightmap(Composition parentLayout, String name) {
		super(parentLayout, name);
	}
	
	public void applyValues(int x, int z, float[][] values) {
		int changeWidth = values.length;
		int changeLength = values[0].length;
		
		for(int u = 0; u < changeWidth; u++) {
			for(int v = 0; v < changeLength; v++) {
				applyValueAt(x+u, z+v, values[u][v]);
			}
		}
	}
	
	public void applyValueAt(int x, int z, float value) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		HeightmapRegion region = regionmap.get(regionX + "," + regionZ);
		
		if(region == null) {
			region = new HeightmapRegion(regionX, regionZ);
			regionmap.put(regionX + "," + regionZ, region);
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		
		region.regionValues[rx][rz] = value;
		region.queRebuildRender();
	}
	
	public void applyChanges(int x, int z, float[][] changes, float factor) {
		int changeWidth = changes.length;
		int changeLength = changes[0].length;
		
		for(int u = 0; u < changeWidth; u++) {
			for(int v = 0; v < changeLength; v++) {
				applyChangeAt(x+u, z+v, changes[u][v], factor);
			}
		}
	}
	
	public void applyChangeAt(int x, int z, float change, float factor) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		HeightmapRegion region = regionmap.get(regionX + "," + regionZ);
		
		if(region == null) {
			region = new HeightmapRegion(regionX, regionZ);
			regionmap.put(regionX + "," + regionZ, region);
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		region.regionValues[rx][rz] = Math.min(1.0f, Math.max(0.0f, region.regionValues[rx][rz] + change * factor));
		region.queRebuildRender();
	}
	
	public float getValueAt(int x, int z) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		HeightmapRegion region = regionmap.get(regionX + "," + regionZ);
		if(region == null) {
			return 0.0f;
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		return region.regionValues[rx][rz];
	}
	
	@Override
	public void writeToFile(File f) {
		
	}
	
	@Override
	public void readFromFile(File f) {
		//TODO Read heightmap layer from file
	}
}
