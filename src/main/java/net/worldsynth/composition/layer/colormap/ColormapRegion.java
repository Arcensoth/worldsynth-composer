/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.colormap;

public class ColormapRegion {
	
	public final int regionX, regionZ;
	public float[][][] regionValues = new float[256][256][3];
	
	private boolean rebuildRenderQued = true;
	
	public ColormapRegion(int regionX, int regionZ) {
		this.regionX = regionX;
		this.regionZ = regionZ;
		
		queRebuildRender();
	}
	
	public final int getRegionX() {
		return regionX;
	}
	
	public final int getRegionZ() {
		return regionZ;
	}
	
	public final float[] getColorAt(int localX, int localZ) {
		return regionValues[localX][localZ];
	}
	
	public final void queRebuildRender() {
		rebuildRenderQued = true;
	}
	
	public final void edqueRebuildRender() {
		rebuildRenderQued = false;
	}
	
	public final boolean regionRebuildRenderIsQued() {
		return rebuildRenderQued;
	}
}
